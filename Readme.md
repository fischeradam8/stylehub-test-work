# Test Job for StyleHub Platform Developer Candidates
## Requirements:
    - docker
    - a way to read makefiles

## How to install:
    - type `make install`

## How to use:
    - type `make start`
    - after the containers are up, you can start sending requests: check localhost:8000/api/ for details or use one of the example commands below
    - type `make stop` to stop all containers
## Available commands:
    - `make cs`
        - runs a php Cs-Fixer fix and a php static analysis via PHPStan 
        - configuration files are located in /app (.php_cs.dist and phpstan.neon respectively)
        - runs locally - requires PHP
#### 
    - `make test`
        - runs all available unit tests 
        - runs locally - requires PHP
#### 
    - `make debug`
        - run the containers in the foreground which lets you see the stdout/stderr from the containers
####    
    - `make example-post`
        - uses cUrl to send a post request containing a product 
#### 
    - `make id=* example-get`
        - uses cUrl to send a get request for the product-request with the provided id (ex. id=2)
#### 
    - `make status=* example-status`
        - uses cUrl to send a get request for all product-requests with the provided status (ex. status=3)
        - available statuses:
            - 0: Waiting to be processed
            - 1: Processing
            - 2: Processing failed
            - 3: Processing succeeded 
