<?php

declare(strict_types=1);

namespace App\Tests;

use App\Dto\ProductRequestInput;
use App\Entity\ProductRequest;
use App\Message\ProductMessage;
use PHPUnit\Framework\TestCase;

abstract class ProductTestCase extends TestCase
{
    protected function getProductRequestInput(): ProductRequestInput
    {
        $productRequestInput = new ProductRequestInput();
        $productRequestInput->name = 'TestProductRequest';
        $productRequestInput->imageCredit = [
      'artist' => 'Leonardo Da Vinci',
      'link' => 'https://en.wikipedia.org/wiki/Leonardo_da_Vinci', ];
        $productRequestInput->tags = ['supper', 'last'];
        $productRequestInput->imageUrl = 'https://upload.wikimedia.org/wikipedia/commons/b/b4/Last_Supper_by_Leonardo_da_Vinci.jpg';
        $productRequestInput->filename = 'Last_Supper_by_Leonardo_da_Vinci.jpg';
        $productRequestInput->imageHash = 'acf59094c5fe008880e72aa4a16bd6ee';
        $productRequestInput->productImg = 'painting.jpg';
        $productRequestInput->itemType = 'painting';
        $productRequestInput->added = 1482213983048;
        $productRequestInput->slug = 'last-supper';
        $productRequestInput->description = 'A legendary painting from the high renaissance';
        $productRequestInput->manufacturer = 'Paintbrush & Co.';
        $productRequestInput->price = 999999999;

        return $productRequestInput;
    }

    protected function getProductRequest(): ProductRequest
    {
        $date = new \DateTime();
        $productRequest = new ProductRequest();
        $productRequest->setProductData($this->getProductRequestInput());
        $productRequest->setStatus(ProductRequest::STATUS_WAITING_TO_BE_PROCESSED);
        $productRequest->setCreatedDate($date);
        $productRequest->setModifiedDate($date);

        return $productRequest;
    }

    protected function getProductMessage(): ProductMessage
    {
        $productMessage = new ProductMessage(1, $this->getProductRequestInput());

        return $productMessage;
    }
}
