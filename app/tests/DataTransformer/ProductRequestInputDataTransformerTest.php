<?php

declare(strict_types=1);

namespace App\Tests\DataTransformer;

use App\DataTransformer\ProductRequestInputDataTransformer;
use App\Entity\ProductRequest;
use App\Tests\ProductTestCase;

class ProductRequestInputDataTransformerTest extends ProductTestCase
{
    public function testTransform(): void
    {
        $transformer = $this->getMockBuilder(ProductRequestInputDataTransformer::class)
            ->setMethods(null)
            ->getMock();
        $productRequestInput = $this->getProductRequestInput();
        $transformedProductRequest = $transformer->transform($productRequestInput, ProductRequest::class);

        $this->assertInstanceOf(ProductRequest::class, $transformedProductRequest);
        $this->assertEquals($this->getProductRequest()->getProductData(), $transformedProductRequest->getProductData());
        $this->assertEquals($this->getProductRequest()->getStatus(), $transformedProductRequest->getStatus());
        $this->assertEqualsWithDelta(
            $this->getProductRequest()->getCreatedDate(),
            $transformedProductRequest->getCreatedDate(),
            5
        );
        $this->assertEqualsWithDelta(
            $this->getProductRequest()->getModifiedDate(),
            $transformedProductRequest->getModifiedDate(),
            5
        );
    }
}
