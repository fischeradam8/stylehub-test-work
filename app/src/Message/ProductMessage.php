<?php

declare(strict_types=1);

namespace App\Message;

use App\Dto\ProductRequestInput;

class ProductMessage
{
  /** @var ProductRequestInput $data  */
    private $data;

    /** @var int $id */
    private $id;

    public function __construct(int $id, ProductRequestInput $data)
    {
        $this->id = $id;
        $this->data = $data;
    }

    public function getData(): ProductRequestInput
    {
        return $this->data;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
