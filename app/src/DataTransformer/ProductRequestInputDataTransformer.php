<?php

declare(strict_types=1);

namespace App\DataTransformer;

use ApiPlatform\Core\DataTransformer\DataTransformerInterface;
use App\Entity\ProductRequest;

class ProductRequestInputDataTransformer implements DataTransformerInterface
{
    /**
     * {@inheritdoc}
     */
    public function transform($object, string $to, array $context = []): ProductRequest
    {
        $productRequest = new ProductRequest();
        $productRequest->setProductData($object);
        $productRequest->setCreatedDate(new \DateTime());
        $productRequest->setModifiedDate(new \DateTime());
        $productRequest->setStatus(ProductRequest::STATUS_WAITING_TO_BE_PROCESSED);

        return $productRequest;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsTransformation($data, string $to, array $context = []): bool
    {
        if ($data instanceof ProductRequest) {
            return false;
        }

        return ProductRequest::class === $to && null !== ($context['input']['class'] ?? null);
    }
}
