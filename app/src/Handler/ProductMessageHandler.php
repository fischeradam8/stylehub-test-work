<?php

declare(strict_types=1);

namespace App\Handler;

use App\Entity\Manufacturer;
use App\Entity\Product;
use App\Entity\ProductRequest;
use App\Entity\Type;
use App\Exception\InvalidEntityException;
use App\Exception\InvalidProductException;
use App\Exception\MissingProductRequestException;
use App\Message\ProductMessage;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ProductMessageHandler implements MessageHandlerInterface
{
    /** @var EntityManager $entityManager */
    private $entityManager;

    private $validator;

    private $logger;

    public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
        $this->logger = $logger;
    }

    public function __invoke(ProductMessage $message)
    {
        $this->setRequestStatus(ProductRequest::STATUS_PROCESSING, $message->getId());
        try {
            $product = $this->transformProduct($message);
            $this->validate($product);
            $this->validateByBusinessLogic($product);
            $this->entityManager->persist($product);
            $this->entityManager->flush();
            $this->setRequestStatus(ProductRequest::STATUS_PROCESSING_SUCCEEDED, $message->getId());
            $this->logger->notice('New product created with id: '.$product->getId());
        } catch (ORMException $exception) {
            $this->logger->error('A database error has occurred!', ['exception' => $exception->getMessage()]);
            $this->setRequestStatus(ProductRequest::STATUS_PROCESSING_FAILED, $message->getId());
            throw $exception;
        } catch (InvalidEntityException $exception) {
            $this->logger->error($exception->getMessage());
            $this->setRequestStatus(ProductRequest::STATUS_PROCESSING_FAILED, $message->getId());
            throw $exception;
        } catch (InvalidProductException $exception) {
            $this->logger->error($exception->getMessage());
            $this->setRequestStatus(ProductRequest::STATUS_PROCESSING_FAILED, $message->getId());
            throw $exception;
        }
    }

    private function transformProduct(ProductMessage $data): Product
    {
        $data = $data->getData();
        /** @var Manufacturer $manufacturer */
        $manufacturer = $this->getManufacturer($data->manufacturer);

        $itemType = $this->getType($data->itemType);

        //Convert from Javascript timestamp to Unix timestamp
        $date = new DateTime();
        $date->setTimestamp(intval($data->added / 1000));

        //TODO add ProductFactory in case Product can be created some other way
        $product = new Product();
        $product->setName($data->name);
        $product->setDescription($data->description);
        $product->setSlug($data->slug);
        $product->setCreatedDate($date);
        $product->setModifiedDate($date);
        $product->setManufacturerId($manufacturer->getId());
        $product->setPrice($data->price);
        $product->setItemTypeId($itemType->getId());
        $product->setImageCredit($data->imageCredit);
        $product->setTags($data->tags);
        $product->setImageUrl($data->imageUrl);
        $product->setFilename($data->filename);
        $product->setImageHash($data->imageHash);
        $product->setProductImg($data->productImg);

        return $product;
    }

    private function setRequestStatus(int $status, int $id): void
    {
        /** @var ProductRequest $productRequest */
        $productRequest = $this->entityManager->getRepository(ProductRequest::class)->find($id);
        if (!isset($productRequest)) {
            $this->logger->error('Missing ProductRequest for ProductMessage with id:'.$id);
            throw new MissingProductRequestException('Could not find ProductRequest with id: '.$id);
        }
        $productRequest->setStatus($status);
        $productRequest->setModifiedDate(new DateTime());
        $this->entityManager->persist($productRequest);
        $this->entityManager->flush();
    }

    private function validate($entity): void
    {
        $violations = $this->validator->validate($entity);
        if ($violations->count() > 0) {
            $displayableViolations = [];
            for ($i = 0; $i < $violations->count(); ++$i) {
                $violation = $violations->get($i);
                $displayableViolations[] =
            'Invalid property: '.$violation->getPropertyPath().' - reason: '.$violation->getMessage();
            }
            throw new InvalidEntityException(implode(PHP_EOL, $displayableViolations));
        }
    }

    private function getManufacturer(string $manufacturerName): Manufacturer
    {
        $manufacturerRepository = $this->entityManager->getRepository(Manufacturer::class);
        /** @var Manufacturer $manufacturer */
        $manufacturer = $manufacturerRepository->findOneBy(['name' => $manufacturerName]);
        if (!isset($manufacturer)) {
            $date = new DateTime();
            $manufacturer = new Manufacturer();
            $manufacturer->setName($manufacturerName);
            $manufacturer->setCreatedDate($date);
            $manufacturer->setModifiedDate($date);

            $this->validate($manufacturer);
            $this->entityManager->persist($manufacturer);
            $this->entityManager->flush();
            $this->logger->notice('Created new manufacturer with id: '.$manufacturer->getId());
        }

        return $manufacturer;
    }

    private function getType(string $typeName): Type
    {
        $typeRepository = $this->entityManager->getRepository(Type::class);
        /** @var Type $type */
        $type = $typeRepository->findOneBy(['name' => $typeName]);
        if (!isset($type)) {
            $date = new DateTime();
            $type = new Type();
            $type->setName($typeName);
            $type->setCreatedDate($date);
            $type->setModifiedDate($date);

            $this->validate($type);
            $this->entityManager->persist($type);
            $this->entityManager->flush();
            $this->logger->notice('Created new type with id: '.$type->getId());
        }

        return $type;
    }

    private function validateByBusinessLogic(Product $product): void
    {
        //Add your extra business logic here. Currently used to alter success ratio to 1:3.
        if (1 !== rand(0, 2)) {
            $message = 'Invalid random number';
            throw new InvalidProductException('Could not create new product! Reason: '.$message);
        }
    }
}
