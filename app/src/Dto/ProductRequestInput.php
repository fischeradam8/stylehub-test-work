<?php

declare(strict_types=1);

namespace App\Dto;

class ProductRequestInput
{
    public $name;

    public $imageCredit;

    public $tags;

    public $imageUrl;

    public $filename;

    public $imageHash;

    public $productImg;

    public $itemType;

    public $added;

    public $slug;

    public $description;

    public $manufacturer;

    public $price;
}
