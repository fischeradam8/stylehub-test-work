<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\ProductRequest;
use App\Message\ProductMessage;
use Doctrine\DBAL\Exception\DatabaseObjectNotFoundException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;

class CreateProduct
{
    /** @var EntityManager */
    private $entityManager;

    private $logger;

    public function __construct(EntityManagerInterface $entityManager, LoggerInterface $logger)
    {
        $this->entityManager = $entityManager;
        $this->logger = $logger;
    }

    public function __invoke(ProductRequest $data, MessageBusInterface $bus): Response
    {
        try {
            $this->entityManager->persist($data);
            $this->entityManager->flush();
            $bus->dispatch(new ProductMessage($data->getId(), $data->getProductData()));
            $responseText = 'Event successfully created with id: '.$data->getId();
            $this->logger->notice($responseText);
            $status = 202;
        } catch (ORMException | DatabaseObjectNotFoundException $exception) {
            $this->logger
          ->error('A mysql error has happened while trying to save a new ProductRequest! Exception: '.$exception->getMessage());
            $responseText = 'We couldn\'t accept your request - please try again later!';
            $status = 500;
        }

        return new Response(json_encode($responseText), $status);
    }
}
