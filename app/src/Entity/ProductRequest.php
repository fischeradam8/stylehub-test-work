<?php

declare(strict_types=1);

namespace App\Entity;

use App\Dto\ProductRequestInput;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class ProductRequest
{
    public const STATUS_WAITING_TO_BE_PROCESSED = 0;
    public const STATUS_PROCESSING = 1;
    public const STATUS_PROCESSING_FAILED = 2;
    public const STATUS_PROCESSING_SUCCEEDED = 3;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="object")
     */
    private $product_data;

    /**
     * @ORM\Column(type="integer", length=2)
     * @Assert\Choice(callback="getStatusCodes")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", nullable=false )
     */
    private $created_date; //TODO move these to abstract parent

    /**
     * @ORM\Column(type="datetime", nullable=false )
     */
    private $modified_date;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return ProductRequestInput
     */
    public function getProductData(): ProductRequestInput
    {
        return $this->product_data;
    }

    /**
     * @param ProductRequestInput $product_data
     */
    public function setProductData(ProductRequestInput $product_data): void
    {
        $this->product_data = $product_data;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate(): DateTime
    {
        return $this->created_date;
    }

    /**
     * @param DateTime $created_date
     */
    public function setCreatedDate(DateTime $created_date): void
    {
        $this->created_date = $created_date;
    }

    /**
     * @return DateTime
     */
    public function getModifiedDate(): DateTime
    {
        return $this->modified_date;
    }

    /**
     * @param DateTime $modified_date
     */
    public function setModifiedDate(DateTime $modified_date): void
    {
        $this->modified_date = $modified_date;
    }

    public static function getStatusCodes(): array
    {
        return [0, 1, 2, 3];
    }
}
