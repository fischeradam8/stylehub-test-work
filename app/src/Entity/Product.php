<?php

declare(strict_types=1);

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @UniqueEntity(
 *   fields={"name", "manufacturer_id"}
 * )
 * @UniqueEntity("slug")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\Column(type="datetime", nullable=false )
     */
    private $created_date; //TODO move these to abstract parent

    /**
     * @ORM\Column(type="datetime", nullable=false )
     */
    private $modified_date;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank
     * @ORM\ManyToOne(targetEntity="App\Entity\Manufacturer", inversedBy="products")
     */
    private $manufacturer_id;

    /**
     * @ORM\Column(type="float", nullable=false)
     * @Assert\NotBlank
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\NotBlank
     * @ORM\ManyToOne(targetEntity="App\Entity\Type", inversedBy="products")
     */
    private $item_type_id;

    /**
     * @ORM\Column(type="object")
     */
    private $image_credit;

    /**
     * @ORM\Column(type="array")
     */
    private $tags;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image_url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $filename;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $imageHash;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $product_img;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @return DateTime
     */
    public function getCreatedDate(): DateTime
    {
        return $this->created_date;
    }

    /**
     * @return DateTime
     */
    public function getModifiedDate(): DateTime
    {
        return $this->modified_date;
    }

    /**
     * @return int
     */
    public function getManufacturerId(): int
    {
        return $this->manufacturer_id;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return int
     */
    public function getItemTypeId(): int
    {
        return $this->item_type_id;
    }

    /**
     * @return array|null
     */
    public function getImageCredit(): ?array
    {
        return $this->image_credit;
    }

    /**
     * @return array|null
     */
    public function getTags(): ?array
    {
        return $this->tags;
    }

    /**
     * @return string|null
     */
    public function getImageUrl(): ?string
    {
        return $this->image_url;
    }

    /**
     * @return string|null
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    /**
     * @return string|null
     */
    public function getImageHash(): ?string
    {
        return $this->imageHash;
    }

    /**
     * @return string|null
     */
    public function getProductImg(): ?string
    {
        return $this->product_img;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @param DateTime $created_date
     */
    public function setCreatedDate(DateTime $created_date): void
    {
        $this->created_date = $created_date;
    }

    /**
     * @param DateTime $modified_date
     */
    public function setModifiedDate(DateTime $modified_date): void
    {
        $this->modified_date = $modified_date;
    }

    /**
     * @param int $manufacturer_id
     */
    public function setManufacturerId(int $manufacturer_id): void
    {
        $this->manufacturer_id = $manufacturer_id;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @param int $item_type_id
     */
    public function setItemTypeId(int $item_type_id): void
    {
        $this->item_type_id = $item_type_id;
    }

    /**
     * @param array $image_credit
     */
    public function setImageCredit(array $image_credit): void
    {
        $this->image_credit = $image_credit;
    }

    /**
     * @param array $tags
     */
    public function setTags(array $tags): void
    {
        $this->tags = $tags;
    }

    /**
     * @param string $image_url
     */
    public function setImageUrl(string $image_url): void
    {
        $this->image_url = $image_url;
    }

    /**
     * @param string $filename
     */
    public function setFilename(string $filename): void
    {
        $this->filename = $filename;
    }

    /**
     * @param string $imageHash
     */
    public function setImageHash(string $imageHash): void
    {
        $this->imageHash = $imageHash;
    }

    /**
     * @param string $product_img
     */
    public function setProductImg(string $product_img): void
    {
        $this->product_img = $product_img;
    }
}
