.PHONY: cs test
cs:
	cd app ; bin/php-cs-fixer-v2.phar fix --verbose --show-progress=estimating
	cd app ; vendor/bin/phpstan analyse src tests --level 7

test:
	cd app ; bin/phpunit

install:
	docker-compose build
	docker-compose run --no-deps php composer install

start:
	docker-compose up -d

stop:
	docker-compose down

debug:
	docker-compose up

example-post:
	curl -X POST -H "Content-Type: application/json" --data @example_product.json http://localhost:8000/api/product/

example-get:
	curl http://localhost:8000/api/product/$(id)

example-status:
	curl http://localhost:8000/api/products?status=$(status)
